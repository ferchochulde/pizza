const pgPromise = require('pg-promise');

const pgp = pgPromise({});

const config = {
    host: "localhost",
    port: "5432",
    database: "pizza",
    user: "postgres",
    password: ""
}

const db = pgp(config);

db.any('SELECT * FROM pizza').then(res => {console.log(res);});
